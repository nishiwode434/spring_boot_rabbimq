package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruductorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruductorApplication.class, args);
	}
}
