package com.example.demo.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * pruductor class
 *
 * @author wujinsheng
 * @date 2018/1/17
 */
@Configuration
public class RabbitConfig {

    @Bean
    public Queue helloQueue() {
//        public Queue(String name, boolean durable, boolean exclusive, boolean autoDelete) {
//            this(name, durable, exclusive, autoDelete, (Map)null);
//        }
        return new Queue("wjs");
    }

}