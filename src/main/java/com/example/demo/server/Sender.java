package com.example.demo.server;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * pruductor class
 *
 * @author wujinsheng
 * @date 2018/1/17
 */
@Component
public class Sender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send() {
        String context = "hello ------wjs------ " + new Date();
        System.out.println("Sender : " + context);
        this.rabbitTemplate.convertAndSend("wjs", context);
    }

}