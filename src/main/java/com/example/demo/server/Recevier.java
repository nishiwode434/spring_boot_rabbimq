package com.example.demo.server;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * pruductor class
 *
 * @author wujinsheng
 * @date 2018/1/17
 */
@Component
public class Recevier {

    @RabbitListener(queues = "wjs")
    @RabbitHandler
    public void process(String hello) {
        System.out.println("Receiver : wjs---->" + hello);
    }

}
