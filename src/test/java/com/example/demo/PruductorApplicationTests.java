package com.example.demo;

import com.example.demo.server.Sender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PruductorApplication.class)
public class PruductorApplicationTests {

	@Autowired
	private Sender helloSender;

	@Test
	public void testRabbit() {
		helloSender.send();
	}

}
